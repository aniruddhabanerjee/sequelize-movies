const express = require('express')
const fs = require('fs')
const path = require('path')
const db = require('../models')
const logger = require('../log/logger')

const route = express.Router()

function getUniqueActor(data) {
  const actor = data.map((element) => element.Actor)
  const uniqueActor = [...new Set(actor)]
  let act_id = 1
  const actor_with_id = uniqueActor.map((element) => {
    let obj = {}
    obj.id = act_id
    obj.name = element
    act_id++
    return obj
  })

  return actor_with_id
}

function getUniqueDirector(data) {
  const director = data.map((element) => element.Director)
  const uniqDirector = [...new Set(director)]
  let dir_id = 1
  const director_with_id = uniqDirector.map((element) => {
    let obj = {}
    obj.id = dir_id
    obj.name = element
    dir_id++
    return obj
  })
  return director_with_id
}

function getUniqueGenre(data) {
  const genre = data.map((element) => element.Genre)
  const uniqGenre = [...new Set(genre)]
  let genre_id = 1
  const genre_with_id = uniqGenre.map((element) => {
    let obj = {}
    obj.id = genre_id
    obj.name = element
    genre_id++
    return obj
  })
  return genre_with_id
}

route.get('/data', (req, res) => {
  fs.readFile(
    path.join(__dirname, '..', 'data', 'movies.json'),
    'utf-8',
    (err, data) => {
      if (err) {
        logger.log('error', err)

        res
          .status(404)
          .json({
            error: 'File Not Found :(',
          })
          .end()
      } else {
        logger.log('info', data)

        data = JSON.parse(data)

        const uniqueActor = getUniqueActor(data)
        const uniqueDirector = getUniqueDirector(data)
        const uniqueGenre = getUniqueGenre(data)

        const updatedMovies = data.map((element) => {
          const actor_id = uniqueActor.find((act) => act.name === element.Actor)
          const director_id = uniqueDirector.find(
            (dir) => dir.name === element.Director
          )
          const genre_id = uniqueGenre.find(
            (genre) => genre.name === element.Genre
          )

          element.ActorId = actor_id.id
          element.DirectorId = director_id.id
          element.GenreId = genre_id.id

          delete element.Actor
          delete element.Director
          delete element.Genre

          return element
        })

        db.Movies.bulkCreate(updatedMovies)
          .then(() => {
            logger.log('info', 'Movies Data Insertion Completed!!! ;)')

            res
              .status(201)
              .json({
                message: 'Movies Data Insertion Completed!!! ;)',
              })
              .end()
          })
          .catch((err) => {
            logger.log('error', err)

            res
              .status(400)
              .json({
                message: 'Movies Data Insertion Failed!!! :(',
              })
              .end()
          })
      }
    }
  )
})

route.get('/actor', (req, res) => {
  fs.readFile(
    path.join(__dirname, '..', 'data', 'movies.json'),
    'utf-8',
    (err, data) => {
      if (err) {
        logger.log('error', err)

        res
          .status(404)
          .json({
            error: 'File Not Found :(',
          })
          .end()
      } else {
        logger.log('info', data)

        data = JSON.parse(data)
        const actors = getUniqueActor(data)

        db.Actor.bulkCreate(actors)
          .then(() => {
            res
              .status(201)
              .json({
                message: 'Actors Data Insertion Completed!!! ;)',
              })
              .end()
          })
          .catch((err) => {
            logger.log('error', err)

            res
              .status(201)
              .json({
                message: 'Actors Data Insertion Failed!!! :(',
              })
              .end()
          })
      }
    }
  )
})

route.get('/director', (req, res) => {
  fs.readFile(
    path.join(__dirname, '..', 'data', 'movies.json'),
    'utf-8',
    (err, data) => {
      if (err) {
        logger.log('error', err)

        res
          .status(404)
          .json({
            error: 'File Not Found :(',
          })
          .end()
      } else {
        logger.log('info', data)

        data = JSON.parse(data)

        const directors = getUniqueDirector(data)

        db.Director.bulkCreate(directors)
          .then(() => {
            res
              .status(201)
              .json({
                message: ' Directors Data Insertion Completed!!! ;)',
              })
              .end()
          })
          .catch((err) => {
            logger.log('error', err)

            res
              .status(400)
              .json({
                message: ' Directors Data Insertion Failed!!! ;)',
              })
              .end()
          })
      }
    }
  )
})

route.get('/genre', (req, res) => {
  fs.readFile(
    path.join(__dirname, '..', 'data', 'movies.json'),
    'utf-8',
    (err, data) => {
      if (err) {
        logger.log('error', err)

        res
          .status(404)
          .json({
            error: 'File Not Found :(',
          })
          .end()
      } else {
        logger.log('info', data)

        data = JSON.parse(data)
        const genre = getUniqueGenre(data)

        db.Genre.bulkCreate(genre)
          .then(() => {
            res
              .status(201)
              .json({
                message: 'Genre Data Insertion Completed!!! ;)',
              })
              .end()
          })
          .catch((err) => {
            logger.log('error', err)

            res
              .status(400)
              .json({
                message: 'Genre Data Insertion Failed :(',
              })
              .end()
          })
      }
    }
  )
})

module.exports = route
