const express = require('express')
const moviesController = require('../controller/moviesController')
const route = express.Router()

route.get('/', moviesController.getAllMovies)

route.post('/', moviesController.createMovie)

route.get('/:id', moviesController.getMovieById)

route.put('/:id', moviesController.updateMovie)

route.delete('/:id', moviesController.deleteMovieById)

route.delete('/', moviesController.deleteAllMovies)

module.exports = route
