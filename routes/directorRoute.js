const express = require('express')
const directorController = require('../controller/directorController')
const route = express.Router()

route.get('/', directorController.getAllDirectors)

route.get('/:id', directorController.getDirectorById)

route.put('/:id', directorController.updateDirectorById)

route.delete('/:id', directorController.deleteDirectorById)

route.post('/', directorController.createDirector)

route.delete('/', directorController.deleteAllDirector)

module.exports = route
