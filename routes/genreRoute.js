const express = require('express')
const genreController = require('../controller/genreController')
const route = express.Router()

route.get('/', genreController.getAllGenre)

route.get('/:id', genreController.getGenreById)

route.put('/:id', genreController.updateGenre)

route.delete('/:id', genreController.deleteGenreById)

route.post('/', genreController.createGenre)

route.delete('/', genreController.deleteAllGenre)

module.exports = route
