const express = require('express')
const userController = require('../controller/userController')

const route = express.Router()

route.get('/', userController.getUser)

route.get('/:id', userController.getUserById)

route.put('/:id', userController.updateUser)

route.delete('/:id', userController.deleteUser)

module.exports = route
