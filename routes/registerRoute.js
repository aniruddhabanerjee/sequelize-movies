const express = require('express')
const registerController = require('../controller/registerController')

const route = express.Router()
route.post('/', registerController.registerUser)

module.exports = route
