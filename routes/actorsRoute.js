const express = require('express')
const actorController = require('../controller/actorController')

const route = express.Router()

route.get('/', actorController.getAllActors)

route.get('/:id', actorController.getActorById)

route.put('/:id', actorController.updateActor)

route.delete('/:id', actorController.deleteActorById)

route.post('/', actorController.createActor)

route.delete('/', actorController.deleteAllActor)

module.exports = route
