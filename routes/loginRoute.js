const express = require('express')
const loginController = require('../controller/loginController')
const route = express.Router()

route.post('/', loginController.loginUser)

module.exports = route
