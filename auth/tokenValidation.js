const jwt = require('jsonwebtoken')
const { access_token } = require('../config')

module.exports = {
  checkToken: (req, res, next) => {
    let token = req.headers['authorization']
    if (token) {
      token = token.split(' ')[1]
      jwt.verify(token, access_token, (err, user) => {
        if (err) {
          res
            .status(400)
            .json({
              message: 'Invalid Token',
            })
            .end()
        } else {
          req.user = user
          next()
        }
      })
    } else {
      res
        .status(401)
        .json({
          message: 'Access denied unauthorized user',
        })
        .end()
    }
  },
}
