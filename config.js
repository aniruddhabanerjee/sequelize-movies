const dotenv = require('dotenv')
dotenv.config()

module.exports = {
  port: process.env.PORT,
  db_username: process.env.DB_USERNAME,
  db_password: process.env.DB_PASSWORD,
  db_host: process.env.DB_HOST,
  db_database: process.env.DB_DATABASE,
  access_token: process.env.ACCESS_TOKEN,
}
