const { Genre } = require('../models')
const logger = require('../log/logger')

const genre = {
  getAllGenre(req, res) {
    Genre.findAll()
      .then((genre) => {
        if (genre.length > 0) {
          res.status(200).json(genre).end()
        } else {
          logger.log('info', `Genre not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Genre not found!!!! :(`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error in Fetching Genre',
          })
          .end()
      })
  },
  getGenreById(req, res) {
    const id = Number(req.params.id)

    Genre.findByPk(id)
      .then((genre) => {
        if (genre) {
          res
            .status(200)
            .json({
              genre,
            })
            .end()
        } else {
          logger.log('info', `Genre with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Genre with the id ${id} is not found!!!! :(`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error Fetching Genre with id=' + id,
          })
          .end()
      })
  },
  updateGenre(req, res) {
    const id = Number(req.params.id)
    Genre.update(req.body, {
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          res
            .status(201)
            .json({
              message: 'Genre was updated successfully.',
            })
            .end()
        } else {
          logger.log('info', `Genre with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Cannot update Genre with id=${id}. Maybe Genre was not found or req.body is empty!`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error updating Genre with id=' + id,
          })
          .end()
      })
  },
  deleteGenreById(req, res) {
    const id = Number(req.params.id)
    Genre.destroy({
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          res
            .status(200)
            .json({
              message: 'Genre was deleted successfully!',
            })
            .end()
        } else {
          logger.log('info', `Genre with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Cannot delete Genre with id=${id}. Genre was not found!`,
            })
            .end()
        }
      })
      .catch((error) => {
        logger.log('error', error)

        res
          .status(500)
          .json({
            message: 'Could not delete Genre with id=' + id,
          })
          .end()
      })
  },
  createGenre(req, res) {
    if (!req.body.id || !req.body.name) {
      res.status(400).send({
        message: 'Content can not be empty!',
      })
      return
    }

    const genre_details = {
      id: req.body.id,
      name: req.body.name,
    }

    Genre.create(genre_details)
      .then((genre) => {
        res
          .status(201)
          .json({
            genre,
          })
          .end()
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message:
              err.message || 'Some error occurred while creating the Genre.',
          })
          .end()
      })
  },
  deleteAllGenre(req, res) {
    Genre.destroy({
      where: {},
      truncate: false,
    })
      .then((num) => {
        res
          .status(200)
          .send({
            message: `${num} Genre were deleted successfully!`,
          })
          .end()
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .send({
            message:
              err.message || 'Some error occurred while removing all Genre.',
          })
          .end()
      })
  },
}

module.exports = genre
