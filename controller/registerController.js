const { Registration } = require('../models')
const logger = require('../log/logger')
const bcrypt = require('bcrypt')

// @desc   Register user
// @route POST /login
// @access Public

const register = {
  registerUser(req, res) {
    if (
      !req.body.id ||
      !req.body.firstname ||
      !req.body.lastname ||
      !req.body.gender ||
      !req.body.email ||
      !req.body.password
    ) {
      res.status(400).send({
        message: 'Content can not be empty!',
      })
      return
    }

    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Failed in Bcrypt',
          })
          .end()
      } else {
        bcrypt.hash(req.body.password, salt, function (err, hash) {
          if (err) {
            logger.log('error', err)

            res
              .status(500)
              .json({
                message: 'Failed in Encryption',
              })
              .end()
          } else {
            const user_details = {
              id: req.body.id,
              firstname: req.body.firstname,
              lastname: req.body.lastname,
              gender: req.body.gender,
              email: req.body.email,
              password: hash,
            }

            Registration.create(user_details)
              .then((user) => {
                res
                  .status(201)
                  .json({
                    user,
                  })
                  .end()
              })
              .catch((err) => {
                logger.log('error', err)

                res
                  .status(500)
                  .json({
                    message:
                      err.message ||
                      'Some error occurred while creating the User.',
                  })
                  .end()
              })
          }
        })
      }
    })
  },
}

module.exports = register
