const logger = require('../log/logger')
const { Director } = require('../models')

const director = {
  getAllDirectors(req, res) {
    Director.findAll()
      .then((actor) => {
        if (actor.length > 0) {
          res.status(200).json(actor).end()
        } else {
          logger.log('info', `Director not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Director not found!!!! :(`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error in Fetching Directors',
          })
          .end()
      })
  },
  getDirectorById(req, res) {
    const id = Number(req.params.id)

    Director.findByPk(id)
      .then((director) => {
        if (director) {
          res
            .status(200)
            .json({
              director,
            })
            .end()
        } else {
          logger.log('info', `Director with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Director with the id ${id} is not found!!!! :(`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error Fetching Director with id=' + id,
          })
          .end()
      })
  },
  updateDirectorById(req, res) {
    const id = Number(req.params.id)
    Director.update(req.body, {
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          res
            .status(201)
            .json({
              message: 'Director was updated successfully.',
            })
            .end()
        } else {
          logger.log('info', `Director with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Cannot update Director with id=${id}. Maybe Director was not found or req.body is empty!`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error updating Director with id=' + id,
          })
          .end()
      })
  },
  deleteDirectorById(req, res) {
    const id = Number(req.params.id)
    Director.destroy({
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          res
            .status(200)
            .json({
              message: 'Director was deleted successfully!',
            })
            .end()
        } else {
          logger.log('info', `Director with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Cannot delete Director with id=${id}. Director was not found!`,
            })
            .end()
        }
      })
      .catch((error) => {
        logger.log('error', error)

        res
          .status(500)
          .json({
            message: 'Could not delete Director with id=' + id,
          })
          .end()
      })
  },
  createDirector(req, res) {
    if (!req.body.id || !req.body.name) {
      res.status(400).send({
        message: 'Content can not be empty!',
      })
      return
    }

    const director_details = {
      id: req.body.id,
      name: req.body.name,
    }

    Director.create(director_details)
      .then((director) => {
        res
          .status(201)
          .json({
            director,
          })
          .end()
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message:
              err.message || 'Some error occurred while creating the Director.',
          })
          .end()
      })
  },
  deleteAllDirector(req, res) {
    Director.destroy({
      where: {},
      truncate: false,
    })
      .then((num) => {
        res
          .status(200)
          .send({
            message: `${num} Director were deleted successfully!`,
          })
          .end()
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .send({
            message:
              err.message || 'Some error occurred while removing all Director.',
          })
          .end()
      })
  },
}

module.exports = director
