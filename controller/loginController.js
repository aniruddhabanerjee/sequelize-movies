const { Registration } = require('../models')
const logger = require('../log/logger')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')

dotenv.config()

// @desc   Auth user and get token
// @route POST /login
// @access Public

const login = {
  loginUser(req, res) {
    if (!req.body.password || !req.body.email) {
      res
        .status(400)
        .send({
          message: 'Email and Password can not be empty!',
        })
        .end()
      return
    }

    Registration.findOne({ where: { email: req.body.email } })
      .then((user) => {
        if (user) {
          bcrypt.compare(
            req.body.password,
            user.password,
            function (err, result) {
              if (err) {
                logger.log('error', err)
                res
                  .status(500)
                  .json({
                    message: 'Failed in comparing password',
                  })
                  .end()
                return
              }
              if (result) {
                const userID = {
                  id: user.id,
                  email: user.email,
                  name: `${user.firstname} ${user.lastname}`,
                }
                const jsonToken = jwt.sign(userID, process.env.ACCESS_TOKEN, {
                  expiresIn: '1d',
                })
                res
                  .status(200)
                  .json({
                    message: 'Login Successful',
                    token: jsonToken,
                  })
                  .end()
              } else {
                res
                  .status(401)
                  .json({
                    message: 'Incorrect Password',
                  })
                  .end()
              }
            }
          )
        } else {
          res
            .status(404)
            .json({
              message: 'User Not Found',
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res.status(500).json({
          message: 'Error in Retrieving user',
        })
      })
  },
}

module.exports = login
