const { Registration } = require('../models')
const logger = require('../log/logger')
const bcrypt = require('bcrypt')

const users = {
  getUser(req, res) {
    Registration.findAll()
      .then((user) => {
        if (user.length > 0) {
          res
            .status(200)
            .json({
              loggedInUser: req.user.name,
              user,
            })
            .end()
        } else {
          logger.log('info', `User not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `User not found!!!! :(`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error in Fetching Users',
          })
          .end()
      })
  },
  getUserById(req, res) {
    const id = Number(req.params.id)

    Registration.findByPk(id)
      .then((user) => {
        if (user) {
          res
            .status(200)
            .json({
              user,
            })
            .end()
        } else {
          logger.log('info', `User with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `User with the id ${id} is not found!!!! :(`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error Fetching User with id=' + id,
          })
          .end()
      })
  },
  updateUser(req, res) {
    const id = Number(req.params.id)

    if (!req.body.password) {
      res
        .status(400)
        .send({
          message: 'Password can not be empty!',
        })
        .end()
      return
    }

    bcrypt
      .hash(req.body.password, 10)
      .then((hash) => {
        req.body.password = hash

        Registration.update(req.body, {
          where: { id: id },
        })
          .then((num) => {
            if (num == 1) {
              res
                .status(201)
                .json({
                  message: 'User was updated successfully.',
                })
                .end()
            } else {
              logger.log('info', `User with the id ${id} is not found!!!! :(`)

              res
                .status(404)
                .json({
                  message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`,
                })
                .end()
            }
          })
          .catch((err) => {
            logger.log('error', err)

            res
              .status(500)
              .json({
                message: 'Error updating User with id=' + id,
              })
              .end()
          })
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error in Encrypting Password',
          })
          .end()
      })
  },
  deleteUser(req, res) {
    const id = Number(req.params.id)
    Registration.destroy({
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          res
            .status(200)
            .json({
              message: 'User was deleted successfully!',
            })
            .end()
        } else {
          logger.log('info', `User with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Cannot delete User with id=${id}. User was not found!`,
            })
            .end()
        }
      })
      .catch((error) => {
        logger.log('error', error)

        res
          .status(500)
          .json({
            message: 'Could not delete User with id=' + id,
          })
          .end()
      })
  },
}

module.exports = users
