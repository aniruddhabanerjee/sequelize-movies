const { Actor } = require('../models')
const logger = require('../log/logger')

const actor = {
  // @desc   Get all the Actors
  // @route  GET /actor/
  // @access Private

  getAllActors(req, res) {
    Actor.findAll()
      .then((actor) => {
        if (actor.length > 0) {
          res.status(200).json(actor).end()
        } else {
          logger.log('info', `Actor not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Actor not found!!!! :(`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error in Fetching Actors',
          })
          .end()
      })
  },
  // @desc   Get Actor By ID
  // @route  GET /actor/:id
  // @access Private

  getActorById(req, res) {
    const id = Number(req.params.id)

    Actor.findByPk(id)
      .then((actor) => {
        if (actor) {
          res
            .status(200)
            .json({
              actor,
            })
            .end()
        } else {
          logger.log('info', `Actor with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Actor with the id ${id} is not found!!!! :(`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error Fetching Actor with id=' + id,
          })
          .end()
      })
  },

  // @desc   Update Actor details By ID
  // @route  PUT /actor/:id
  // @access Private

  updateActor(req, res) {
    const id = Number(req.params.id)
    Actor.update(req.body, {
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          res
            .status(201)
            .json({
              message: 'Actor was updated successfully.',
            })
            .end()
        } else {
          logger.log('info', `Actor with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Cannot update Actor with id=${id}. Maybe Actor was not found or req.body is empty!`,
            })
            .end()
        }
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message: 'Error updating Actor with id=' + id,
          })
          .end()
      })
  },
  // @desc   Delete Actor details By ID
  // @route  DELETE /actor/:id
  // @access Private

  deleteActorById(req, res) {
    const id = Number(req.params.id)
    Actor.destroy({
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          res
            .status(200)
            .json({
              message: 'Actor was deleted successfully!',
            })
            .end()
        } else {
          logger.log('info', `Actor with the id ${id} is not found!!!! :(`)

          res
            .status(404)
            .json({
              message: `Cannot delete Actor with id=${id}. Actor was not found!`,
            })
            .end()
        }
      })
      .catch((error) => {
        logger.log('error', error)

        res
          .status(500)
          .json({
            message: 'Could not delete Actor with id=' + id,
          })
          .end()
      })
  },
  // @desc   Create Actor
  // @route  POST /actor/
  // @access Private

  createActor(req, res) {
    if (!req.body.id || !req.body.name) {
      res.status(400).send({
        message: 'Content can not be empty!',
      })
      return
    }

    const actor_details = {
      id: req.body.id,
      name: req.body.name,
    }

    Actor.create(actor_details)
      .then((actor) => {
        res
          .status(201)
          .json({
            actor,
          })
          .end()
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .json({
            message:
              err.message || 'Some error occurred while creating the Actor.',
          })
          .end()
      })
  },
  // @desc   DELETE all Actors
  // @route  DELETE /actor
  // @access Private

  deleteAllActor(req, res) {
    Actor.destroy({
      where: {},
      truncate: false,
    })
      .then((num) => {
        res
          .status(200)
          .send({
            message: `${num} Actors were deleted successfully!`,
          })
          .end()
      })
      .catch((err) => {
        logger.log('error', err)

        res
          .status(500)
          .send({
            message:
              err.message || 'Some error occurred while removing all Actors.',
          })
          .end()
      })
  },
}

module.exports = actor
