const express = require('express')
const path = require('path')
const db = require('./models')
const moviesRoute = require('./routes/moviesRoute')
const actorsRoute = require('./routes/actorsRoute')
const directorRoute = require('./routes/directorRoute')
const insertRoute = require('./routes/insertRoute')
const genreRoute = require('./routes/genreRoute')
const userRoute = require('./routes/userRoute')
const loginRoute = require('./routes/loginRoute')
const registerRoute = require('./routes/registerRoute')
const logger = require('./log/logger')
const { checkToken } = require('./auth/tokenValidation')
const { port } = require('./config')
const app = express()

app.use(express.json())

app.use(express.static(path.join(__dirname, 'public')))

app.use('/login', loginRoute)

app.use('/register', registerRoute)

app.use('/user', checkToken, userRoute)

app.use('/insert', checkToken, insertRoute)

app.use('/movies', checkToken, moviesRoute)

app.use('/actor', checkToken, actorsRoute)

app.use('/director', checkToken, directorRoute)

app.use('/genre', checkToken, genreRoute)

app.use((req, res) => {
  res.status(404)
  res
    .json({
      error: {
        message: 'Page Not Found',
      },
    })
    .end()
})

db.sequelize
  .sync()
  .then(() => {
    app.listen(port, () => {
      logger.log('info', `Server Running on ${port}...`)
    })
  })
  .catch((err) => {
    logger.log('error', 'Unable to connect to the database:' + err)
  })
