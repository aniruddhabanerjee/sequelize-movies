module.exports = (sequelize, DataTypes) => {
  const Movies = sequelize.define('Movies', {
    Rank: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
    },
    Title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Description: {
      type: DataTypes.STRING(1234),
      allowNull: false,
    },
    Runtime: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Rating: {
      type: DataTypes.FLOAT,
    },
    Metascore: {
      type: DataTypes.STRING,
    },
    Votes: {
      type: DataTypes.INTEGER,
    },
    Gross_Earning_in_Mil: {
      type: DataTypes.STRING,
    },
    Year: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  })
  return Movies
}
