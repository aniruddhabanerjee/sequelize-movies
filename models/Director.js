module.exports = (sequelize, DataTypes) => {
  const Director = sequelize.define('Director', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  })
  return Director
}
