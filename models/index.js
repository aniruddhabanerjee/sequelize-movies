const Sequelize = require('sequelize')
const { db_database, db_host, db_password, db_username } = require('../config')
const db = {}

let sequelize = new Sequelize(db_database, db_username, db_password, {
  host: db_host,
  dialect: 'mysql',
})

db.sequelize = sequelize
db.Sequelize = Sequelize

db.Movies = require('./Movies')(sequelize, Sequelize)
db.Actor = require('./Actor')(sequelize, Sequelize)
db.Director = require('./Director')(sequelize, Sequelize)
db.Genre = require('./Genre')(sequelize, Sequelize)
db.Registration = require('./Registration')(sequelize, Sequelize)

db.Actor.hasMany(db.Movies)
db.Director.hasMany(db.Movies)
db.Genre.hasMany(db.Movies)

module.exports = db
